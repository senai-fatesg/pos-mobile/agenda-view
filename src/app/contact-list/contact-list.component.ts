import { Component, OnInit } from '@angular/core';
import { ContactService } from '../services/contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  contacts;
  selectedContact;

  constructor(private dataService: ContactService) { }

  ngOnInit() {
    this.dataService.getContacts().subscribe(r => {
      this.contacts = r;
    });
  }

  public selectContact(contact) {
    this.selectedContact = contact;
  }

}
