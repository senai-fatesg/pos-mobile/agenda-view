import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) { }

  public getContacts():Observable<any>{
    return this.http.get("http://localhost:8080/agenda/contact");
  }
  public createContact(contact: {id, name, description, email}){
    return this.http.post("http://localhost:8080/agenda/contact", contact);  
  }

}
